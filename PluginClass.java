

package com.fretzealot.fzunity;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import com.fz.blelib.LEDBLELib;
import com.fz.blelib.LEDBLELibCallback;
import com.fz.blelib.UFZCallback;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;
import java.util.ArrayList;
import java.util.List;


public class PluginClass extends UnityPlayerActivity

{
    private static PluginClass m_instance;
    private LEDBLELib mLib;
    private List<BluetoothDevice> mLeDevices;


    public Context uContext;
    public Activity uActivity;

    public static PluginClass instance()
    {

        if(m_instance == null)
            m_instance = new PluginClass();


        m_instance.initFZ();

        return m_instance;
    }


    private void initFZ()
    {

        uActivity =UnityPlayer.currentActivity;
        uContext = uActivity.getApplicationContext();


               mLib = LEDBLELib.getInstance(uContext, new UFZCallback() {
                   @Override
                   public void onStartScan() {
                       ULog("cbGeneral", "onStartScan");
                   }

                   @Override
                   public void onStopScan() {
                       ULog("cbGeneral", "onStopScan");
                   }

                   @Override
                   public void onConnecting(String s) {
                       ULog("cbGeneral", "onConnecting");
                   }

                   @Override
                   public void onConnected(String s) {
                       ULog("cbGeneral", "onConnected");
                   }

                   @Override
                   public void onDisconnected() {
                       ULog("cbGeneral", "onDisconnected");
                   }

                   @Override
                   public void onDeviceFound(String s, String s1) {
                       ULog("cbGeneral", "onDeviceFound");
                   }

                   @Override
                   public void onBTBroadcastMessage(String s) {
                       ULog("cbGeneral", "BTBroadcastMessage: " + s);
                   }


               });


               ULog("cbGeneral", "init");

    }

    public void startScan()
    {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ActivityCompat.checkSelfPermission(UnityPlayer.currentActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(UnityPlayer.currentActivity,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_LOCATION);

            }
            else
            {
                INT_startScan();
            }

        }
        else
        {
            INT_startScan();
        }
    }

    public void INT_startScan()
    {
        if (UnityPlayer.currentActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            mLeDevices = new ArrayList<>();
            mLeDevices.clear();
            ULog("cbGeneral","Scanning...");

            mLib.startScan(leScanCallback);
        }
        else
        {
            ULog("cbNotSupported","FEATURE_BLUETOOTH_LE is not supported");
        }
    }

    public void clear()
    {
        mLib.clear();
    }

    public void sendCommandFlush()
    {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                mLib.sendCommandFlush();
            }
        });
    }

    public void Resume()
    {
        mLib.onResume();
    }

    public void Pause()
    {
        if (mLib.isConnected())
            mLib.onPause();
    }

    public void sendCommandClear()
    {
        mLib.sendCommandBufferClear();
    }

    public boolean isConnected()
    {
        return  mLib.isConnected();
    }

    public void set(int _fretID, int _stringID,int _R, int _G, int _B, int _intensity, int _fademode)
    {
        mLib.set((byte) _fretID,(byte)_stringID, (byte)_R,(byte)_G,(byte)_B,(byte)_intensity,(byte)_fademode);
    }

    public void setAll(int _R, int _G, int _B, int _intensity, int _fademode)
    {
        mLib.set_all((byte)_R,(byte)_G,(byte)_B,(byte)_intensity,(byte)_fademode);
    }

    public void setAcross( int _stringID,int _R, int _G, int _B, int _intensity, int _fademode)
    {
        if(_stringID<0 || _stringID>5)
            return;

        mLib.set_across((byte)_stringID, (byte)_R,(byte)_G,(byte)_B,(byte)_intensity,(byte)_fademode);
    }

    public void setSubset(int _fretStart, int _stringID,int _R, int _G, int _B, int _intensity, int _fademode)
    {
        mLib.set_subset((byte) _fretStart,(byte)_stringID, (byte)_R,(byte)_G,(byte)_B,(byte)_intensity,(byte)_fademode);
    }

    public void connect(String dAddress) {
       // mLib.disconnect();

        mLib.startService(dAddress, new LEDBLELibCallback() {

            @Override
            public void onConnected() {
                ULog("cbConnected","YES");
            }

            @Override
            public void onDisconnected() {
                try {
                    mLib.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ULog("cbDisconnected","");

            }

            @Override
            public void onServiceDiscovered(List<BluetoothGattService> serviceList) {
                try {
                    ULog("cbGeneral","onServiceDiscovered");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onDataReceived(byte[] rxBytes) {

            }

            @Override
            public void onBatteryString(String s) {

            }

            @Override
            public void onManufactureNameString(String s) {

            }

            @Override
            public void onModelNumberString(String s) {

            }

            @Override
            public void onSerialNumberString(String s) {

            }

            @Override
            public void onHardwareRevisionString(String s) {

            }
        });
    }

    public void disconnect()
    {
        try {
            mLib.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback()
    {
       @Override
       public void onLeScan (final BluetoothDevice device, int rssi, byte[] scanRecord) {

                   UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   if (!mLeDevices.contains(device) && mLeDevices.size()==0)
                       if (device.getName() != null)
                           if (device.getName().toLowerCase().contains("fret zealot")) {
                               mLeDevices.add(device);
                               ULog("cbScanCompleted", device.getName() + "|" + device.getAddress()+";");
                           }
               }
           });
       }

    };



    void ULog(String mtype, String msg)
    {
        UnityPlayer.UnitySendMessage("ETLFretZealot", mtype, msg);
    }



}
