


#import "ETLFretZealot.h"

extern "C"
{
    
#ifndef MakeStringCopy
    // Converts NSString to C style string by way of copy (Mono will free it)
#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL
#endif
    
#ifndef GetStringParam
    // Converts C style string to NSString
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]
#endif
 
void NATIVE_Init()
{
    [[ETLFretZealot sharedInstance] FZInit];
}
  
void NATIVE_startScan()
{
    [[ETLFretZealot sharedInstance] startScan];
}

void NATIVE_connect(char* DeviceID)
{
    [[ETLFretZealot sharedInstance] connect:GetStringParam(DeviceID)];
}

void NATIVE_disconnect()
{
    [[ETLFretZealot sharedInstance] disconnect];
}

void NATIVE_clear()
{
    [[ETLFretZealot sharedInstance] clear];
}

void NATIVE_sendCommandFlush()
{
    [[ETLFretZealot sharedInstance] sendCommandFlush];
}
    
void NATIVE_sendCommandClear()
{
    [[ETLFretZealot sharedInstance] sendCommandClear];
}

BOOL NATIVE_isConnected()
{
    return [[ETLFretZealot sharedInstance] isConnected];
}

void NATIVE_set(int fretID, int _stringID, int _R, int _G, int _B, int _intensity, int _fademode)
{
    [[ETLFretZealot sharedInstance] set:fretID
                              stringID:_stringID
                                     R:_R
                                     G:_G
                                     B:_B
                             intensity:_intensity
                              fadeMode:_fademode];
    
}
    
void NATIVE_setAll (int _R, int _G, int _B, int _intensity, int _fademode)
{
    [[ETLFretZealot sharedInstance] setAll:_R
                                        G:_G
                                        B:_B
                                intensity:_intensity
                                 fadeMode:_fademode];
}

    
void NATIVE_setAcross(int _stringID, int _R, int _G, int _B, int _intensity, int _fademode)
{
     [[ETLFretZealot sharedInstance] setAcross:_stringID
                                            R:_R
                                            G:_G
                                            B:_B
                                    intensity:_intensity
                                     fadeMode:_fademode];
}
    
void NATIVE_setSubset(int fretStart, int _stringID, int _R, int _G, int _B, int _intensity, int _fademode)
{
    [[ETLFretZealot sharedInstance] setSubset:fretStart
                                    stringID:_stringID
                                           R:_R
                                           G:_G
                                           B:_B
                                   intensity:_intensity
                                    fadeMode:_fademode];
}
    
    
}
