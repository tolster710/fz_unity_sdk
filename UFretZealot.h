

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BlueTooth.h"


@interface ETLFretZealot : NSObject<CBCentralManagerDelegate>

@property (nonatomic) CBManagerState BTState;
@property (nonatomic, strong) CBCentralManager* bluetoothManager;
@property (nonatomic, strong ) NSMutableArray* deviceArray;


(ETLFretZealot *)sharedInstance;

(void)sendMessage:(NSString*)type message:(NSString*)_message;
(void)FZCreate;
(void)FZInit;
(void)startScan;
(void)connect:(NSString*)DeviceID;
(void)disconnect;
(void)clear;
(void)sendCommandFlush;
(void)sendCommandClear;
(BOOL)isConnected;

(void)set:(int)fretID
  stringID:(int)_stringID
         R:(int)_R
         G:(int)_G
         B:(int)_B
 intensity:(int)_intensity
  fadeMode:(int)_fademode;

(void)setAll:(int)_R
            G:(int)_G
            B:(int)_B
    intensity:(int)_intensity
     fadeMode:(int)_fademode;

(void)setAcross:(int)_stringID
               R:(int)_R
               G:(int)_G
               B:(int)_B
       intensity:(int)_intensity
        fadeMode:(int)_fademode;

(void)setSubset:(int)fretStart
        stringID:(int)_stringID
               R:(int)_R
               G:(int)_G
               B:(int)_B
       intensity:(int)_intensity
        fadeMode:(int)_fademode;

@end
