# fz_unity_sdk

Resources / Examples for leveraging Fret Zealot as a plugin with Unity


## References

Unity:
https://docs.unity3d.com/Manual/Plugins.html

iOS:
https://twnkls.com/blogs/howto-native-ios-plugins-for-unity3d/
https://docs.unity3d.com/Manual/PluginsForIOS.html


Android:
https://twnkls.com/en/blogs/howto-native-android-plugins-for-unity3d-2/
https://docs.unity3d.com/Manual/PluginsForAndroid.html
https://docs.unity3d.com/Manual/AndroidUnityPlayerActivity.html



Descriptions:
UFretZealot.cs: Static Unity C# Script. Communication with native plugins. 

UFretZealot.h,
UFretZealot.m: iOS manager class includes helper methods for the communication of Fret Zealot iOS SDK.
UFretZealotBridge.mm : Required for using native iOS methods in Unity.

PluginClass.java : Android manager class includes helper methods for communication of Fret Zealot Android SDK. (Must be compiled with Android Studio to .JAR or .AAR file)

[Android SDK](https://github.com/edgetechlabs/fz-android-sdk)
[iOS SDK](https://github.com/edgetechlabs/fz-ios-sdk)

Usage:

Create new Empty GameObject in Unity and Attact UFretZealot.cs file on it.

Create new C# script connect to Fret Zealot devive and call this:
```
    //Am Chord
    //public List<int> Fingers = new List<int>{2,3,1};
    //public List<string> Frets = new List<string>{"x","0","2","2","1","0"};

#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
        if (UFretZealot.inst.isConnected)
            UFretZealot.inst.ShowChord(Fingers, Frets);
#endif
```


## Unity Directory Structure

```bash
├── Assets
│   ├── Plugins
│   │   ├── Android
│   │   │   ├── FretZealot.jar
│   │   │   └── UFZBridge.jar
│   │   └── iOS
│   │       ├── BlueTooth.h
│   │       ├── Bluetooth.m
│   │       ├── UFretZealotBridge.mm
│   │       ├── UFretZealot.h
│   │       └── UFretZealot.m
│   └── Scripts
│       └── UFretZealot.cs
```

