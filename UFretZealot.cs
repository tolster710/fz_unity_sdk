

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ETLFretZealot : MonoBehaviour
{
    public static ETLFretZealot inst = null;

#if UNITY_ANDROID
    private static AndroidJavaObject _fzPlugin;
    private static string fullClassName = "com.fretzealot.fzunity.PluginClass";
    private static AndroidJavaObject activity;
#endif

    public event Action<bool> OnDeviceConnected;

    public event Action OnDeviceDisconnected;

    public event Action<List<FZDevice>> OnScanCompleted;

    public event Action OnScanStopped;

    public List<FZDevice> DeviceList = new List<FZDevice>();

    public bool isConnected = false;

    [Range(0, 10)]
    public int intensity = 10;

    [Range(0, 4)]
    public int fade = 0;

    public string BTState;


    private void Awake()
    {

        if (inst != null)
            Debug.LogError("ETLFretZealot: Multiple instances not allowed!");

        if (gameObject.name != "ETLFretZealot")
            gameObject.name = "ETLFretZealot";

        //initCurrentState();
        inst = this;
    }

#if UNITY_ANDROID && !UNITY_EDITOR
    private void OnApplicationPause(bool pauseStatus)
    {
        if (_fzPlugin != null)
        {
            ScheduledTask(.5f, () => _fzPlugin.Call(pauseStatus ? "Pause" : "Resume"));
        }
    }
#endif

    private void Start()
    {
#if !UNITY_EDITOR
#if UNITY_IOS

        ScheduledTask(1f, () => FZ_Init());
#endif

#if UNITY_ANDROID

        using (var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        }

        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            using (var AJavaClass = new AndroidJavaClass(fullClassName))
            {
                _fzPlugin = AJavaClass.CallStatic<AndroidJavaObject>("instance");
                ScheduledTask(1f, () => _fzPlugin.Call("Resume"));
         }
        }));

#endif
#endif
    }

    public void ShowChord(List<int> fingers, List<string> frets)
    {
        PrepareFZ();

        var vFingers = new List<int>();
        vFingers.AddRange(fingers);

        var vFrets = new List<string>();
        vFrets.AddRange(frets);

        var fID = 0;
        for (int i = 0; i < vFrets.Count; i++)
        {
            var fingerID = -1;
            var fretID = -1;
            var stringID = i;

            if (!vFrets[i].Equals("0") && !vFrets[i].Equals("x"))
            {
                fingerID = vFingers[fID];
                fID++;

                fretID = Convert.ToInt32(vFrets[i]);
            }
            else
            {
                if (vFrets[i].Equals("0"))
                    fretID = 0;
            }

            //default color open strings
            var c = new FZFingerColor { R = 12, G = 15, B = 15 };

            if (fretID > 0)
                c = GetFingerColor(fingerID);

            if (fretID == -1)
                FZ_setAcross(stringID, 4, 0, 0, intensity, fade);
            else
                FZ_set(fretID, stringID, c.R, c.G, c.B, stringID == 0 ? 2 : intensity, fade);
        }

        FlushFZ();
    }

    public void PrepareFZ()
    {
        FZ_sendCommandClear();
        FZ_clear();
#if UNITY_IOS
        FZ_sendCommandFlush();
#endif
    }

    public void FlushFZ()
    {
#if UNITY_IOS
        FZ_sendCommandFlush();
#elif UNITY_ANDROID
       ScheduledTask(.5f,()=>FZ_sendCommandFlush());
#endif
    }

    #region FretZealot Public Methods

    public void FZ_Init()
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_Init();
#endif

#if UNITY_ANDROID && UNITY_EDITOR
        _fzPlugin.Call("Resume");
#endif
    }

    public void FZ_startScan()
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_startScan();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("startScan");
#endif
    }

    public void FZ_connectFirst()
    {
#if UNITY_IOS && !UNITY_EDITOR
        if (DeviceList.Count > 0)
            NATIVE_connect(DeviceList[0].DeviceAddress);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        if (DeviceList.Count > 0)
            _fzPlugin.Call("connect", DeviceList[0].DeviceAddress);
#endif
    }

    public void FZ_connect(string DeviceID)
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_connect(DeviceID);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("connect", DeviceID);
#endif
    }

    public void FZ_disconnect()
    {
#if UNITY_IOS && !UNITY_EDITOR
        if (isConnected)
        {
            PrepareFZ();
            ScheduledTask(.5f, () => NATIVE_disconnect());
        }
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        if (isConnected)
        {
            PrepareFZ();

            ScheduledTask(.5f, () => _fzPlugin.Call("disconnect"));
        }
#endif
    }

    public void FZ_set(int fretID, int stringID, int R, int G, int B, int intensity, int fadeMode)
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_set(fretID, stringID, R, G, B, intensity, fadeMode);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("set", fretID, stringID, R, G, B, intensity, fadeMode);
#endif
    }

    public void FZ_setAll(int R, int G, int B, int intensity, int fadeMode)
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_setAll(R, G, B, intensity, fadeMode);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("setAll", R, G, B, intensity, fadeMode);
#endif
    }

    public void FZ_setAcross(int stringID, int R, int G, int B, int intensity, int fadeMode)
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_setAcross(stringID, R, G, B, intensity, fadeMode);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("setAcross", stringID, R, G, B, intensity, fadeMode);
#endif
    }

    public void FZ_setSubset(int fretStart, int stringID, int R, int G, int B, int intensity, int fadeMode)
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_setSubset(fretStart, stringID, R, G, B, intensity, fadeMode);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("setSubset", fretStart, stringID, R, G, B, intensity, fadeMode);
#endif
    }

    public void FZ_clear()
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_clear();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("clear");
#endif
    }

    public void FZ_sendCommandFlush()
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_sendCommandFlush();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        _fzPlugin.Call("sendCommandFlush");
#endif
    }

    public void FZ_sendCommandClear()
    {
#if UNITY_IOS && !UNITY_EDITOR
        NATIVE_sendCommandClear();
#endif

#if UNITY_ANDROID
        _fzPlugin.Call("sendCommandClear");
#endif
    }

    public bool FZ_isConnected()
    {
#if UNITY_IOS && !UNITY_EDITOR
        return NATIVE_isConnected();
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        return _fzPlugin.Call<bool>("isConnected");
#endif

        return false;
    }

    #endregion FretZealot Public Methods

    #region Callback Methods

    public void cbConnected(string msg)
    {
        Debug.Log("cbConnected: " + msg);
        var isSuccess = msg.Equals("YES");

        isConnected = isSuccess;

        if (isConnected)
            ScheduledTask(.5f, () => PrepareFZ());

        if (OnDeviceConnected != null)
            OnDeviceConnected(isSuccess);
    }

    public void cbDisconnected(string msg)
    {
        Debug.Log("cbDisconnected: " + msg);
        isConnected = false;
        if (OnDeviceDisconnected != null)
            OnDeviceDisconnected();
    }

    public void cbScanCompleted(string msg)
    {
        //msg: Fret Zealot|AC834DE9-C4F8-6CDF-3B2A-XXXXXXX;

        Debug.Log("Scan Completed: " + msg);
        if (msg.Length > 0)
        {
            var aDev = msg.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var dev in aDev)
            {
                Debug.Log("device: " + dev);
                var d = dev.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                var deviceName = d[0];
                var deviceAddress = d[1];
                DeviceList.Add(new FZDevice { DeviceName = deviceName, DeviceAddress = deviceAddress });
            }
        }

        if (OnScanCompleted != null)
            OnScanCompleted(DeviceList);
    }


    public void cbScanStopped()
    {
        if (OnScanStopped != null)
            OnScanStopped();
    }

    public void cbBTStateUpdate(string msg)
    {
        BTState = msg;
        Debug.Log("BTState: " + msg);
    }

    public void cbGeneral(string msg)
    {
        Debug.Log("ETLFretZealot: " + msg);
    }

    public void cbNotSupported(string msg)
    {
        Debug.Log("ETLFretZealot: " + msg);
    }

    public void cbAndroidIntScan(string m)
    {
#if UNITY_ANDROID
        _fzPlugin.Call("INT_startScan");
#endif
    }

    public void cbAndroid(string msg)
    {
#if UNITY_ANDROID
            //TODO:

#endif
    }

    #endregion Callback Methods

    #region UtilMethods

    public void ScheduledTask(float sec, Action action)
    {
        StartCoroutine(_scheduledTask(sec, action));
    }

    private IEnumerator _scheduledTask(float sec, Action action)
    {
        yield return new WaitForSeconds(sec);
        if (action != null)
            action();
    }

    public FZFingerColor GetFingerColor(int fingerID)
    {
        switch (fingerID)
        {
            case 1:
                return new FZFingerColor { R = 0, G = 0, B = 15 };

            case 2:
                return new FZFingerColor { R = 0, G = 15, B = 0 };

            case 3:
                return new FZFingerColor { R = 0, G = 15, B = 15 };

            case 4:
                return new FZFingerColor { R = 12, G = 0, B = 15 };

            default:
                return new FZFingerColor { R = 12, G = 15, B = 15 };
        }
    }

    public FZFingerColor Color32ToFZColor(Color32 col)
    {
        var r = col.r._Map(255, 15);
        var g = col.g._Map(255, 15);
        var b = col.b._Map(255, 15);

        return new FZFingerColor { R = r, G = g, B = b };
    }

    #endregion UtilMethods

    #region iOS FretZealot Native Method Calls

#if UNITY_IOS && !UNITY_EDITOR

    [DllImport("__Internal")]
    private static extern void NATIVE_Init();

    [DllImport("__Internal")]
    private static extern void NATIVE_startScan();

    [DllImport("__Internal")]
    private static extern void NATIVE_connect(string DeviceID);

    [DllImport("__Internal")]
    private static extern void NATIVE_disconnect();

    [DllImport("__Internal")]
    private static extern void NATIVE_clear();

    [DllImport("__Internal")]
    private static extern void NATIVE_sendCommandFlush();

    [DllImport("__Internal")]
    private static extern void NATIVE_sendCommandClear();

    [DllImport("__Internal")]
    private static extern bool NATIVE_isConnected();

    [DllImport("__Internal")]
    private static extern void NATIVE_set(int fretID, int _stringID, int _R, int _G, int _B, int _intensity, int _fademode);

    [DllImport("__Internal")]
    private static extern void NATIVE_setAll(int _R, int _G, int _B, int _intensity, int _fademode);

    [DllImport("__Internal")]
    private static extern void NATIVE_setAcross(int _stringID, int _R, int _G, int _B, int _intensity, int _fademode);

    [DllImport("__Internal")]
    private static extern void NATIVE_setSubset(int fretStart, int _stringID, int _R, int _G, int _B, int _intensity, int _fademode);

#endif

    #endregion iOS FretZealot Native Method Calls
   
}

public struct FZDevice
{
    public string DeviceName;
    public string DeviceAddress;
}

public struct FZFingerColor
{
    public int R;
    public int G;
    public int B;

    public override string ToString()
    {
        return string.Format("R:{0} G:{1} B:{2}", R, G, B);
    }
}

public struct FZMap
{
    public int stringID;
    public List<FZMapItem> items;
}

public struct FZMapItem
{
    public int fretID;
    public FZFingerColor fColor;
}

public static class ByteMapExtension
{
    public static byte _Map(this byte value, byte source, byte destination)
    {
        return (byte)Mathf.RoundToInt((destination * value) / source);
    }
}