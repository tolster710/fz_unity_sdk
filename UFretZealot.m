

#define UNITY 

#ifdef UNITY
#import "UnityAppController.h"
#endif

#import "ETLFretZealot.h"
#import "BlueTooth.h"


#ifdef DEBUG
#define DLog(...) NSLog(__VA_ARGS__)
#else
#define DLog(...) /* */
#endif


BlueTooth *mLib;

@interface ETLFretZealot ()

@end


#pragma mark - Class Variables

static ETLFretZealot *_sharedInstance;


#pragma mark - Class Definition

@implementation ETLFretZealot
@synthesize deviceArray,BTState,bluetoothManager;

(void)sendMessage:(NSString*)type message:(NSString*)_message
{
#if UNITY
      UnitySendMessage("ETLFretZealot", [type UTF8String], [_message UTF8String]);
#else
    DLog(@"%@->%@",type,_message);
#endif
}

(void)FZCreate
{
      mLib = [BlueTooth sharedInstance];
}

(void)FZInit
{
    NSLog(@"FretZealot Init...");
    if(!self.bluetoothManager)
    {
         self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()
                                                                   options:@{CBCentralManagerOptionShowPowerAlertKey: @(NO)}
                                 ];
    }
    [self centralManagerDidUpdateState:self.bluetoothManager]; // Show initial state
    
}

(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
   
    BTState = self.bluetoothManager.state;
    switch(self.bluetoothManager.state)
    {
        case CBCentralManagerStateResetting:
            [self sendMessage:@"cbBTStateUpdate" message:@"Resetting"];
            break;
            
        case CBCentralManagerStateUnsupported:
             [self sendMessage:@"cbBTStateUpdate" message:@"Unsupported"];
            break;
            
        case CBCentralManagerStateUnauthorized:
            [self sendMessage:@"cbBTStateUpdate" message:@"Unauthorized"];
            break;
            
        case CBCentralManagerStatePoweredOff:
            [self sendMessage:@"cbBTStateUpdate" message:@"PoweredOff"];
            break;
            
        case CBCentralManagerStatePoweredOn:
            [self sendMessage:@"cbBTStateUpdate" message:@"PoweredOn"];
            [self FZCreate];
            break;
            
        default:
            break;
    }
    
}

(void)startScan
{
    [mLib startScanDevicesWithInterval:2.5 CompleteBlock:^(NSArray *devices) {
        
        DLog(@"removeall devices ");
        [self.deviceArray removeAllObjects];
        for (CBPeripheral *per in devices)
        {
            if(![self.deviceArray containsObject:per])
            {
                if(per.name.length > 4 && [[per.name substringWithRange:NSMakeRange(0, 4)] isEqualToString:@"Fret"]) // Only shows fret devices
                {
                    [self.deviceArray addObject:per];
                    DLog(@"device added");
                }
                
            }
            
        }
        
        if (self.deviceArray.count==0) {
            DLog(@"scanCompleted NoDeviceFound");
            [self sendMessage:@"cbScanCompleted" message:@""];
        }
        else
        {
            NSMutableString *devices =[NSMutableString string];
            
            for ( CBPeripheral *device in self.deviceArray){
                [devices appendFormat:@"%@|%@;",
                 device.name,
                 [device.identifier UUIDString]];
            }
            
            DLog(@"scanCompleted. Devices -> %@" , devices);

            [self sendMessage:@"cbScanCompleted" message:devices];
        }
       
        
    }];
}


(void)connect:(NSString*)DeviceID
{
    [mLib connectionWithDeviceUUID:DeviceID TimeOut:5 CompleteBlock:^(CBPeripheral *device_new, NSError *err)
     {
         if (device_new)
         {
             NSLog(@"Discovery servicess...");
             [mLib discoverServiceAndCharacteristicWithInterval:3 CompleteBlock:^(NSArray *serviceArray, NSArray *characteristicArray, NSError *err)
              {
                  DLog(@"Device Connected\n\n");
                   [self sendMessage:@"cbConnected" message:@"YES"];
                  
              }];
         }
         else
         {
              DLog(@"Connect device failed.");
              [self sendMessage:@"cbConnected" message:@"NO"];
         }
         
     }];
    
}


(void)disconnect
{
     [mLib disconnectionDevice];
     [self sendMessage:@"cbDisconnected" message:@""];
}


(void)clear
{
    [mLib clear];
}


(void)sendCommandFlush
{
    [mLib sendCommandFlush];
}


(void)sendCommandClear
{
    [mLib sendCommandClear];
}


(BOOL)isConnected
{
    return [mLib isConnection];
}


(void)set:(int)fretID
  stringID:(int)_stringID
         R:(int)_R
         G:(int)_G
         B:(int)_B
 intensity:(int)_intensity
  fadeMode:(int)_fademode
{
    [mLib set:fretID
    withPixel:_stringID
      withRed:_R
        Green:_G
         Blue:_B
withIntensity:_intensity
     withFade:_fademode];
    
}

(void)setAll:(int)_R
            G:(int)_G
            B:(int)_B
    intensity:(int)_intensity
     fadeMode:(int)_fademode
{
    [mLib set_all:_R
            Green:_G
             Blue:_B
    withIntensity:_intensity
         withFade:_fademode];
}

(void)setAcross:(int)_stringID
               R:(int)_R
               G:(int)_G
               B:(int)_B
       intensity:(int)_intensity
        fadeMode:(int)_fademode
{
    if(_stringID<0 || _stringID>5)
        return;

  //  NSLog(@"stringID: %d",_stringID);

    [mLib set_across:_stringID
                 Red:_R
               Green:_G
                Blue:_B
       withIntensity:_intensity
            withFade:_fademode];
    
}


(void)setSubset:(int)fretStart
        stringID:(int)_stringID
               R:(int)_R
               G:(int)_G
               B:(int)_B
       intensity:(int)_intensity
        fadeMode:(int)_fademode
{
    [mLib set_subset:fretStart
           withPixel:_stringID
                 Red:_R
               Green:_G
                Blue:_B
       withIntensity:_intensity
            withFade:_fademode];
    
}

#pragma mark - Constructors

(void)initialize
{
	static BOOL classInitialized = NO;
	
	if (classInitialized == NO)
	{
		_sharedInstance = [[ETLFretZealot alloc] init];

  
    _sharedInstance.deviceArray = [NSMutableArray array];
		classInitialized = YES;
	}
}

(id)allocWithZone: (NSZone *)zone
{
	if (_sharedInstance == nil)
	{
		return [super allocWithZone: zone];
	}
	else
	{
	    return [self sharedInstance];
	}
}

(id)init
{
	if ((self = [super init]) == nil)
	{
		return nil;
	}
	
	return self;
}

(ETLFretZealot *)sharedInstance
{
	return _sharedInstance;
}

(id)copyWithZone: (NSZone *)zone
{
	return self;
}

@end
